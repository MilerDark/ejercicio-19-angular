import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {
peliculas!:any;
series!:any;

  constructor() { }

  ngOnInit(): void {
  }

  recibirObjeto1($event:any):void{
    this.peliculas = $event;
    console.log(this.peliculas);
  }

  recibirObjeto2($event:any):void{
    this.series = $event;
    console.log(this.series);
    
  }
}

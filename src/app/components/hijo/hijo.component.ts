import { Component, OnInit } from '@angular/core';
import { Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  Objeto1 = {
    nombre: "Hasta el ultimo hombre",
    genero: "Guerra",
    tipo:"Accion"
  };

  Objeto2 = {
    nombre: "Dota 2",
    genero: "Videojuegos",
    tipo:"Dibujo animado"
  }

  @Output() EmitirObjeto1 = new EventEmitter<any>();
  @Output() EmitirObjeto2 = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
    this.EmitirObjeto1.emit(this.Objeto1);
    this.EmitirObjeto2.emit(this.Objeto2)
  }

}
